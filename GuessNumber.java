import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99

		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		boolean condition = false ;
		int guess ; //Read the user input
		int attempt = 0;
		while (!condition){
			guess = reader.nextInt();
			if (guess == -1){
				System.out.println("Sorry, the number was " + number);
				break;
			}
			if (guess == number){
				System.out.println("Congratulations! You won after "+ attempt + " attempts! ");
				condition = true;
			} else if (guess > number){
				System.out.println("Sorry!");
				System.out.println("Mine is less than your guess. ");
				System.out.println("Type -1 to quit or guess another: ");
				attempt++;
			} else if (guess < number){
				System.out.println("Sorry");
				System.out.println("Mine is greater than your guess.");
				System.out.println("Type -1 to quit or guess another: ");
				attempt++;
			}
		}

	}

}