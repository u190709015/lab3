public class FindPrimes {
    public static void main (String[] args) {
        int primenumber = Integer.parseInt(args[0]);
        function(primenumber);
    }
    public static void function(int primenumber) {
        int x, y;
        for (x = 1; x <= primenumber; x++) {
            if (x == 1 || x == 0) {
                continue;
            }
            boolean z = true ;
            for (y = 2; y <= x / 2; ++y) {
                if (x % y == 0) {
                    z = false;
                    break;
                }
            }if(z)
                System.out.print(x + " ");
        }
    }
}
